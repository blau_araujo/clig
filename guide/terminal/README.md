# Conhecer o terminal

- [Atalhos do terminal](guide/terminal/keyboard.md)
- [Alterar o prompt de comandos (bash)](guide/terminal/prompt.md)
