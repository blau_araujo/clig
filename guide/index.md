# Guia interativo do shell do GNU/Linux

## O que você quer fazer?

- [Obter ajuda](help/README.md)
- [Saber mais sobre o GNU](about-gnu/README.md)
- [Saber mais sobre este guia](about-clig/README.md)
- [Saber mais sobre o terminal](terminal/README.md)
- [Gerenciar arquivos](file-management/README.md)
- [Encontrar arquivos](file-find/README.md)
- [Pesquisar textos em arquivos](text-search/README.md)
- [Editar arquivos de texto](text-edit/README.md)
- [Ler ou escrever dados em arquivos](data-io/README.md)
- [Gerenciar usuários](user-management/README.md)
- [Instalar e remover programas](software-management/README.md)
- [Gerenciar programas em execução](proc/README.md)
- [Gerenciar serviços do sistema](system-services/README.md)
- [Obter informações de hardware](hard-info/README.md)
- [Gerenciar unidades de armazenamento](disks/README.md)
- [Gerenciar memória](mem/README.md)
- [Gerenciar CPU](cpu/README.md)
