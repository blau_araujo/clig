# Gerenciar unidades de armazenamento

- [Verificar espaço em disco](guide/disks/usage.md)
- [Montar e desmontar partições](guide/disks/partitions.md)
- [Trabalhar com discos virtuais](guide/disks/virt.md)

