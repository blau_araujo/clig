# Instalar e remover software

- [Compilando fontes](guide/software-management/sources.md)
- [No Debian](guide/software-management/debian.md)
- [No Arch](guide/software-management/arch.md)
- [No Fedora](guide/software-management/fedora.md)
- [No CentOS](guide/software-management/centos.md)
- [No Open SUSE](guide/software-management/suse.md)
