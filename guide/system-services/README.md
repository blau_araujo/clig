# Gerenciar serviços do sistema

- [Reiniciar ou desligar o sistema](guide/system-services/shutdown.md)
- [Iniciar, parar ou reiniciar serviços](guide/system-services/systemctl.md)
- [Gerenciar serviços de áudio (pulse)](guide/system-services/pulse.md)
- [Gerenciar sessões gráficas do X.org](guide/system-services/xorg.md)
