# Listar arquivos e diretórios

## Sintaxe geral

```
ls [OPÇÕES] [CAMINHOS]
```

### Opções mais comuns

```
ls      Listar arquivos e diretórios no caminho atual.
ls -a   Incluir arquivos e diretórios ocultos na listagem.
ls -l   Exibir listagem no formato detalhado.
ls -la  Incluir arquivos ocultos na lista detalhada.
```

> 1. No GNU/Linux, diretórios também são arquivos.

> 2. Arquivos e diretórios ocultos são aqueles com nomes
>    iniciados com um ponto (.).

## Listar nomes de arquivos por padrões

Os CAMINHOS em `ls` podem ser expandidos pelo shell:

```
*               Zero ou mais caracteres.
?               Exatamente um caractere.
[CARACTERES]    Um caractere da lista.
```

Exemplos:

```
ls *.txt        Lista todos os arquivos terminados em `.txt'.

ls a[lb]?cate   Lista todos os arquivos iniciados com `a',
                seguido de `l' ou `b', seguido de um caractere
                qualquer e terminados em `cate'.
```

## Técnica recomendada para scripts

```
echo [CAMINHO/]*    Lista arquivos e diretórios em CAMINHO.
echo [CAMINHO/]*/   Lista apenas diretórios em CAMINHO.
```

Para listar um arquivo ou diretório por linha:

```
printf '%s\n' [CAMINHO/]*
printf '%s\n' [CAMINHO/]*/
```

