# Guia interativo da linha de comando (clig)

Versão de desenvolvimento!

## Instalação

Ainda não há um instalador!

### Para testar...

**Depende do Bash e do GNU coreutils!**

- Instale o programa `fzf` (fuzzy-finder).
- Clone o repositório, entre no diretório criado e execute:

```
./clig     # Abre o guia interativo.
./clig -h  # Ajuda sobre o uso.
./clig -v  # Versão e licença.
```

**Nem todos os arquivos `.md` foram atualizado para a nova versão!**

## Páginas do guia

O `clig` foi idealizado para encontrar páginas de guia na seguinte estrutura:

```
clig/guides
     |
     +--- tópico-1
     |       |
     |       +--- README.md
     |       |
     |       +--- guia1.md
     |       |
     |       +--- guia2.md
     |       |
     |      ...
     |
     +--- tópico-2
     |       |
    ...     ...
     |
     +--- tópico-n
     |       |
     |      ...
     |
     +--- index.md
```

### index.md

Links para os arquivos `README.md` de todos os diretórios dos tópicos.

### tópico/README.md

Links para todos os arquivos de guia no diretório do tópico.

## Em implementação

- Uma opção de linha de comando para listar todos os guias disponíveis diretamente no menu inicial.
- Opções de linha de comando para alterar o tamanho e a posição da pré-visualização.
- Uma opção de linha de comando para informar caminhos de outros guias (imagine um guia de estudos para certificação LPIC ou uma versão interativa dos meus livros!).
- Opção de carregar um guia online.

## Screenshots

Menu inicial:

![](screenshots/ss01.png)

Pré-visualização do guia:

![](screenshots/ss02.png)

Guia aberto no paginador (less):

![](screenshots/ss03.png)

## Como colaborar

Antes de propor *pull requests*, apresente suas ideias e correções nas [issues](https://codeberg.org/blau_araujo/clig/issues).

## Para apoiar o meu trabalho

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)
