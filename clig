#!/usr/bin/env bash
# ----------------------------------------------------------------------------
# Script   : clig
# Descrição: Command line interactive guide
# Versão   : 0.0.4
# Date     : 09/05/2022
# License  : GNU/GPL v3.0
# ----------------------------------------------------------------------------
# Copyright (C) 2022, Blau Araujo <blau@debxp.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
# ----------------------------------------------------------------------------
# Usage: clig [OPTIONS]
# ----------------------------------------------------------------------------

version=0.0.4

# options --------------------------------------------------------------------

prev_position=bottom
prev_size=70

# strings --------------------------------------------------------------------

fzf_prompt='O que você quer fazer? '
fzf_header=' '
less_prompt="Press \`Q' to quit, \`H' for help, and SPACE to scroll."

usage=$'\e[1mGuia interativo da linha de comando (clig) '$version$'\e[m

\e[1mUso:\e[m clig [OPÇÕES]

\e[1mOPÇÕES\e[m

  -v, --version Imprime informações de versão e sai.
  -h, --help    Imprime esta ajuda e sai.

\e[1mNAVEGAÇÃO\e[m

  No menu inicial : ESC sai do guia.
  Nos demais menus: ESC volta para o índice anterior.
  Em um guia      : ENTER abre o guia em um paginador (less).
                    CTRL+SETAS rola a visualização.
                    Roda do mouse rola a visualização.
  No paginador    : Q sai do paginador.
'

copy=$'\e[1mCommand line interactive guide (clig) '$version$'\e[m

Copyright (C) 2022, Blau Araujo <blau@debxp.org>
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Blau Araujo.
'

# error messages -------------------------------------------------------------

err_msg[0]='Saindo...'
err_msg[1]='Dependências não encontrada (fzf)!'
err_msg[2]='Argumento inválido!'

# fzf options ----------------------------------------------------------------

fzf_opts=(
    --layout=reverse
    -e
    -i
    --info=hidden
    --tiebreak=begin
    "--prompt=$fzf_prompt"
    "--header=$fzf_header"
    "--preview-window=$prev_position:$prev_size%"
    '--preview=map_guides $f; md_parse $p/$(get_guide {})'
)

# functions ------------------------------------------------------------------

md_parse() {
    local h h1 h2 h3 p li quote code reset line
    # Color scheme...
    h1=$'\e[94;1m'
    h2=$'\e[34;1m'
    h3=$'\e[34;1m'
    p=$'\e[33m'
    li=$'\e[37m'
    quote='\e[95;3m'
    code=$'\e[97;2m'
    reset=$'\e[m'
    # Iterate lines...
    while IFS= read line || [[ $line ]]; do
        mark=${line%% *}
        case $mark in
            \#*) # Processa cabeçalhos

                 [[ ${#mark} -le 3 ]] && h="h${#mark}" || h=h3
                 line="${line#* }"
                 case $h in
                     h1) line="${line^^}";;
                     h2) line="• $line";;
                     h3) line="  $line";;
                 esac
                 printf "${!h}%s$reset\n" "$line"
                 ;;
           -|\*) # Processa listas...
                 if [[ "$line" =~ ^-\ \[(.*)\]\(.*\).*$ ]]; then
                     line="${BASH_REMATCH[1]}"
                 else
                     line=${line#* }
                 fi
                 printf "$li  • %s$reset\n" "$line"
                 ;;
          '```') # Processa blocos de código...
                 while :; do
                     read -r
                     [[ "$REPLY" = '```' ]] && break
                     printf "$code  %s$reset\n" "$REPLY"
                 done
                 ;;
            '>') # Processa citações;;;
                 printf "  $quote%s$reset\n" "${line#* }"
                 ;;
              *) # Processa outras marcações de linha...
                 printf "$p  %s$reset\n" "$line"
        esac
    done < $1
    echo
}

# Os guias são os arquivos README.md nos diretórios em 'guides'...
map_guides() {
    local sub
    sub='s|- \[(.*)\]\((.*)\)|\2:\1|p'
    mapfile -t pages_map < <(sed -nr "$sub" $1)
}

get_guide() {
    local f i
    i=0
    for f in "${pages_map[@]#*:}"; do
        if [[ "$1" = "$f" ]]; then
            echo ${pages_map[i]%%:*}
            return
        fi
        ((i++))
    done
    return 1
}

fzf_run () {
    f="$dpath/$gfile" p=$dpath fzf "${fzf_opts[@]}"
}


die() {
    printf '%s\n' "${err_msg[$1]}" "${err_msg[0]}"
    exit $1
}

# main -----------------------------------------------------------------------

# Opções da linha de comando...


[[ $1 ]] && case $1 in
       -h|--help)   # Imprime ajuda e sai...
                    echo "$usage"
                    exit
                    ;;
    -v|--version)   # Imprime versão e sai...
                    echo "$copy"
                    exit
                    ;;
               *)   # Argumento inválido...
                    die 2
                    ;;
esac

# Verifica a dependência do fuzzy-finder...
command -v fzf &> /dev/null || die 1

# Exporta as funções para o fzf...
export -f map_guides md_parse get_guide

# Caminho inicial (em desenvolvimento)...
dpath='./guide'

# Arquivo do menu inicial...
gfile='index.md'
cpath=''

while :; do
    # Se não for uma página de menu, exibe o conteúdo e sai...
    [[ $gfile =~ (index|README).md ]] || {
        md_parse $dpath/$gfile | less -r -P "$less_prompt"
        exit
    }
    # Se for uma página de menu, mapeia a lista de links para o fzf...
    map_guides $dpath/$gfile
    sel=$(printf '%s\n' "${pages_map[@]#*:}" | fzf_run)
    # Se algo for selecionado, registra caminho e arquivo...
    if [[ $sel ]]; then
        # Registra caminho e arquivo anteriores...
        ofile=$gfile
        opath=$cpath
        # Caminho do arquivo selecionado...
        gfile=$(get_guide "$sel")
        # Páginas dos guias não têm caminho...
        [[ $gfile == */* ]] && : ${gfile%/*} || : $opath
        cpath=$_
        gfile=$cpath/${gfile##*/}
    else
        # Se nada for selecionado e o arquivo atual for o index, sai...
        [[ $gfile == *index.md ]] && exit
        gfile=$ofile
        cpath=$opath
        continue
    fi
done

